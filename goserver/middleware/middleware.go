package middleware

import (
	"database/sql"
	"net/http"
	"servergpt/controllers"
	"servergpt/models"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)



var reqBody models.ReqBody
var GenericId string


func JWTMiddlewareEnabled() gin.HandlerFunc {
    err := godotenv.Load()
    if err != nil {
        panic("Error loading .env file")
    }

    return func(c *gin.Context) {
       
        err := c.ShouldBindJSON(&reqBody)
        if err != nil {
           // c get url param id
            GenericId = c.Query("id")
        }
        if reqBody.User != "" {
            row , _ := FindUserByID(reqBody.User)
            if !row {
                c.JSON(http.StatusBadRequest, gin.H{"error": "User not found"})
                return
            }
            // inject reqBody to c
            c.Set("user", reqBody)
        }else if GenericId != "" {
            row , _ := FindUserByID(GenericId)
            if !row {
                c.JSON(http.StatusBadRequest, gin.H{"error": "User not found"})
                return
            }
            // inject reqBody to c
            reqBody.User = GenericId
            c.Set("user", reqBody)
        }
        // c.Set("enabled", true)

        c.Next()
    }
    
}

func FindUserByID(id string) (bool, error) {
    var result string
    sqlt := "SELECT id FROM users WHERE id = ? AND enabled = 1;"
    err := controllers.DB.GetConnection().QueryRow(sqlt, id).Scan(&result)
    if err != nil {
        if err == sql.ErrNoRows {
            return false, nil
        }
        return false, err
    }
    return true, nil
}