package main

import (
	"log"

	"servergpt/controllers"
	"servergpt/middleware"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func main() {
	errLoad := godotenv.Load()
	if errLoad != nil {
		log.Fatal("Error loading .env file")
	}

	controllers.DBConnection()

	r := gin.Default()
	// Configuración de CORS
	r.Use(cors.Default())
	//  static files

	r.POST("/signin", controllers.SignIn)

	rEnabled := r.Group( "")
	rEnabled.Use(middleware.JWTMiddlewareEnabled())
	{
		rEnabled.POST("/davinci", controllers.CreateChatCompletion)
		rEnabled.POST("/dalle", controllers.CreateImage)
		rEnabled.GET("/get-rooms", controllers.ShowRoomsByUser)
		rEnabled.GET("/get-messages", controllers.ShowMessagesByRoom)
		rEnabled.DELETE("/delete-room/:id", controllers.RemoveRoom)
		rEnabled.PATCH("/rename-room/:id", controllers.RenameRoom)
	}

	
	r.Run()
}
